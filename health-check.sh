#!/bin/bash

echo ___________________________________
echo Health-check process...
echo ___________________________________

sleep 60;
if [ "$(curl http://localhost:8080/actuator/health)" == '{"status":"UP"}' ] && [ "$(curl -Is http://localhost:8080/actuator/health | grep HTTP/ | awk {'print $2'})" == '200' ]; then
 echo "UP AND RUNNING"
 exit
 else echo "FAIL" ; fi 2>/dev/null