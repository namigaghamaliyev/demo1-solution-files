#!/usr/bin/env bash

sudo apt update --fix-missing -y
sudo apt install openjdk-14-jdk -y

#git clone https://gitlab.com/namigaghamaliyev/demo1.git && cd "$(basename "$_" .git)"
function clone_pull {
    Dir=$(basename "$1" .git)
    if [[ -d "$Dir" ]]; then
      cd $Dir
      git pull
    else
      git clone "$1" && cd $Dir
    fi
}

clone_pull https://gitlab.com/namigaghamaliyev/demo1.git
#git clone https://gitlab.com/namigaghamaliyev/demo1.git && cd "$(basename "$_" .git)"

#make mvnw script file excetuable
chmod +x mvnw
./mvnw clean package

#get the *.jar package from $PROJECT_DIR/target folder and place it in the APP_USER home folder
cp /home/app_user/demo1/target/spring-petclinic-2.3.1.BUILD-SNAPSHOT.jar /home/app_user/spring-petclinic-2.3.1.BUILD-SNAPSHOT.jar