#!/usr/bin/env bash

sudo apt-get update --fix-missing -y && sudo apt-get install -qq mysql-server

green() {
  echo -e '\e[32m'$1'\e[m';
}

#Defining variables
DBNAME=petclinic
DBUSER=petclinic
DBPASSWD=petclinic123456789@
ROOTPASSWD=petclinic123456789@

MYSQL=`which mysql`

# Construct the MySQL query
Q1="CREATE DATABASE IF NOT EXISTS $DBNAME;"
Q2="CREATE USER IF NOT EXISTS '$DBUSER'@'192.168.33.%' IDENTIFIED BY '$DBPASSWD';"
Q3="GRANT ALL ON $DBNAME.* TO '$DBUSER'@'192.168.33.%';"
Q4="FLUSH PRIVILEGES;"
SQL="${Q1}${Q2}${Q3}${Q4}"

# Run the exceute
$MYSQL -uroot -p$ROOTPASSWD -e "$SQL"

# Let the user know the database was created
green "Database $DBNAME and user $DBUSER created with succesfully"

#change bind address for giving global access
sudo sed -i "s/bind-address.*/bind-address = 0.0.0.0/" /etc/mysql/mysql.conf.d/mysqld.cnf

#Restart mysql to made all this stuff affect it
sudo service mysql restart